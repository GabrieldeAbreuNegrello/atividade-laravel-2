<?php


Route::get('/', function () {
    return view('site.home');
})->name('home');

Route::get('/html', function () {
    return view('site.html');
})->name('html');

Route::get('/javascripts', function () {
    return view('site.javascripts');
})->name('javascripts');

Route::get('/dica-css', function () {
    return view('site.css');
})->name('dicas-css');

Route::get('/videosAulas', function () {
    return view('site.videos');
})->name('videosAulas');

Route::get('/contato', function () {
    return view('site.contato');
})->name('contato');