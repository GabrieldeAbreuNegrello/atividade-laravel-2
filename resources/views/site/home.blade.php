@extends('layouts.site')

@section('titulo','html')

@section('conteudo')

<div class="row">
    <div class="col-lg-4 col-md-6 mt-3">
        <img src="img/post-3.jpg" alt="" class="img-fluid">
        <h3>Dicas de HTML</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis laudantium sint nulla delectus officiis, placeat inventore fugiat quia quasi nihil sapiente doloribus expedita molestias, adipisci vero aperiam nisi qui ab?</p>
  </div>
  
  <div class="col-lg-4 col-md-6 mt-3">
        <img src="img/post-4.jpg" alt="" class="img-fluid">
        <h3>O que é MVC</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus qui beatae officia soluta modi non nemo. Explicabo rem ab dicta a, ad nemo consequuntur ratione commodi adipisci, fugiat harum earum!</p>
  </div>
  
  <div class="col-lg-4 col-md-6 mt-3 ">
        <img src="img/post-5.jpg" alt="" class="img-fluid">
        <h3>Planejamento</h3>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui similique suscipit omnis mollitia, dolorum quae aperiam labore veniam perferendis asperiores quidem expedita nesciunt possimus fugiat enim reprehenderit quaerat eligendi officiis!</p>
  </div>

</div><!--/.row -->

<div id="fac">
<h1>FAÇA DIFERENTE...</h1>
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse praesentium quis enim molestiae molestias voluptatem neque, sunt laborum explicabo sit. Aut repellendus, dolorem unde facere rerum distinctio voluptate architecto corrupti!</p>
</div>


@endsection